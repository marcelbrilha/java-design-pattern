package br.com.cod3r.chain.sinister;

import br.com.cod3r.chain.sinister.chain.survey.impl.CRValidateSurveyInsured;
import br.com.cod3r.chain.sinister.chain.survey.impl.CRValidateSurveyThird;

public class Client {

	public static void main(String[] args) {
		CRValidateSurveyThird crValidateSurveyThird = new CRValidateSurveyThird();
		CRValidateSurveyInsured crValidateSurveyInsured = new CRValidateSurveyInsured();

		crValidateSurveyThird.addNext(crValidateSurveyInsured);

		try {
			System.out.println(crValidateSurveyThird.validate(1L));
		} catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
	}

}
