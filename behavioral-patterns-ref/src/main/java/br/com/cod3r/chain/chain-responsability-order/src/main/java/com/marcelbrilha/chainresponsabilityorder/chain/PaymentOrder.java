package com.marcelbrilha.chainresponsabilityorder.chain;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Order(2)
@Service
public class PaymentOrder implements OrderChain {

    @Override
    public String execute() {
        return "Pagamento realizado com sucesso";
    }

}
