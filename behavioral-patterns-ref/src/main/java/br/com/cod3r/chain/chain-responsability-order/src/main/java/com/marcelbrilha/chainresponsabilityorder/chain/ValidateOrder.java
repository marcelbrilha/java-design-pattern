package com.marcelbrilha.chainresponsabilityorder.chain;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Order(1)
@Service
public class ValidateOrder implements OrderChain {

    @Override
    public String execute() {
        return "Validação Realizada";
    }

}
