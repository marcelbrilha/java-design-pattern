package com.marcelbrilha.chainresponsabilityorder.controller;

import com.marcelbrilha.chainresponsabilityorder.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/v1/orders")
    public String executeOrder() {
        return orderService.executeOrder();
    }

}
