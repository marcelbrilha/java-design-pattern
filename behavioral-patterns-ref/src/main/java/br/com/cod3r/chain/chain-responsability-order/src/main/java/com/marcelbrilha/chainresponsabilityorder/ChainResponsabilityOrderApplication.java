package com.marcelbrilha.chainresponsabilityorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChainResponsabilityOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChainResponsabilityOrderApplication.class, args);
	}

}
