package br.com.cod3r.chain.sinister.chain.survey.impl;

import br.com.cod3r.chain.sinister.chain.survey.CRValidateSurvey;

public class CRValidateSurveyThird implements CRValidateSurvey {

	private CRValidateSurvey crValidateSurvey;

	@Override
	public String validate(Long notice) throws Exception {
//		String response = surveyRepository.getThird(notice);
//
//		if (response != null)
//			return response;

		return crValidateSurvey.validate(notice);
	}

	@Override
	public void addNext(CRValidateSurvey crValidateSurvey) {
		this.crValidateSurvey = crValidateSurvey;
	}

}
