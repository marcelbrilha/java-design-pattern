package br.com.cod3r.chain.sinister.chain.survey;

public interface CRValidateSurvey {

	String validate(Long notice) throws Exception;

	void addNext(CRValidateSurvey crValidateSurvey);

}
