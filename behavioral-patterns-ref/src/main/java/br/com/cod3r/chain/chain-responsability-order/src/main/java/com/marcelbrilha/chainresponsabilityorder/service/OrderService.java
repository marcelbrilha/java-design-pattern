package com.marcelbrilha.chainresponsabilityorder.service;

import com.marcelbrilha.chainresponsabilityorder.chain.OrderChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private List<OrderChain> ordersChain;

    public String executeOrder() {
        String response = "";

        for (OrderChain orderChain : ordersChain) {
            String chainResponse = orderChain.execute();

            if (chainResponse != null)
                response = chainResponse;
        }

        return response;
    }

}
