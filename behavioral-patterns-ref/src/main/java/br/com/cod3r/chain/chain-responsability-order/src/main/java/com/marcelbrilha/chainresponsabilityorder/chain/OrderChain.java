package com.marcelbrilha.chainresponsabilityorder.chain;

public interface OrderChain {

    String execute();

}
